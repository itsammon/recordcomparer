/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.homework1.matching;

import ammonclegg.homework1.input.Parser;
import homework1.person.Person;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ammon
 */
public class Children2Test {
    private List<Person> persons = new ArrayList<>();
    
    public Children2Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        persons = Parser.parseFile("TestFiles/PersonTestSet_01.json", "json");
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of match method, of class Children2.
     */
    @Test
    public void testMatchTrue() {
        System.out.println("match");
        Person person1 = persons.get(1);
        Person person2 = persons.get(4);
        Children2 instance = new Children2();
        boolean expResult = true;
        boolean result = instance.match(person1, person2);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of match method, of class Children2.
     */
    @Test
    public void testMatchFalse() {
        System.out.println("match");
        Person person1 = persons.get(0);
        Person person2 = persons.get(3);
        Children2 instance = new Children2();
        boolean expResult = false;
        boolean result = instance.match(person1, person2);
        assertEquals(expResult, result);
    }
}
