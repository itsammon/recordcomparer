/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework1.person;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Class to represent a person
 * @author Ammon Clegg
 */

// Used by jackson parser
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property="__type",

                defaultImpl = Person.class)

@JsonSubTypes({

        @JsonSubTypes.Type(value = Child.class, name = "Child:#PersonMatcher.DataObjects"),

        @JsonSubTypes.Type(value = Adult.class, name = "Adult:#PersonMatcher.DataObjects"),

        @JsonSubTypes.Type(value = Adult.class, name = "Adult"),

        @JsonSubTypes.Type(value = Child.class, name = "Child")}

)
public class Person {
    protected int objectId;
    private String stateFileNumber;
    private String socialSecurityNumber;
    private String firstName;
    private String middleName;
    private String lastName;
    private Integer birthYear;
    private Integer birthMonth;
    private Integer birthDay;
    private String gender;
    
    public int getObjectId()
    {
        return objectId;
    }
    public void setObjectId(int id)
    {
        objectId = id;
    }
    
    public String getStateFileNumber()
    {
        return stateFileNumber;
    }
    public void setStateFileNumber(String num)
    {
        stateFileNumber = num;
    }
    
    public String getSocialSecurityNumber()
    {
        return socialSecurityNumber;
    }
    public void setSocialSecurityNumber(String num)
    {
        socialSecurityNumber = num;
    }
    
    public String getFirstName()
    {
        return firstName;
    }
    public void setFirstName(String name)
    {
        firstName = name;
    }
    
    public String getMiddleName()
    {
        return middleName;
    }
    public void setMiddleName(String name)
    {
        middleName = name;
    }
    
    public String getLastName()
    {
        return lastName;
    }
    public void setLastName(String name)
    {
        lastName = name;
    }
    
    public int getBirthYear()
    {
        return birthYear;
    }
    public void setBirthYear(int year)
    {
        birthYear = year;
    }
    
    public int getBirthMonth()
    {
        return birthMonth;
    }
    public void setBirthMonth(int month)
    {
        birthMonth = month;
    }
    
    public int getBirthDay()
    {
        return birthDay;
    }
    public void setBirthDay(int day)
    {
        birthDay = day;
    }
    
    public String getGender()
    {
        return gender;
    }
    public void setGender(String gen)
    {
        gender = gen;
    }
}
