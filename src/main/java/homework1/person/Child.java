/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework1.person;

/**
 * Class to represent a child
 * @author ammon
 */
public class Child extends Person {
    private String newbornScreeningNumber;
    private String isPartOfMultipleBirth;
    private Integer birthOrder;
    private String birthCounty;
    private String motherFirstName;
    private String motherMiddleName;
    private String motherLastName;
    
    public String getNewbornScreeningNumber()
    {
        return newbornScreeningNumber;
    }
    public void setNewbornScreeningNumber(String num)
    {
        newbornScreeningNumber = num;
    }
    
    public String getIsPartOfMultipleBirth()
    {
        return isPartOfMultipleBirth;
    }
    public void setIsPartOfMultipleBirth(String isPart)
    {
        isPartOfMultipleBirth = isPart;
    }
    
    public int getBirthOrder()
    {
        return birthOrder;
    }
    public void setBirthOrder(int order)
    {
        birthOrder = order;
    }
    
    public String getBirthCounty()
    {
        return birthCounty;
    }
    public void setBirthCounty(String county)
    {
        birthCounty = county;
    }
    
    public String getMotherFirstName()
    {
        return motherFirstName;
    }
    public void setMotherFirstName(String name)
    {
        motherFirstName = name;
    }
    
    public String getMotherMiddleName()
    {
        return motherMiddleName;
    }
    public void setMotherMiddleName(String name)
    {
        motherMiddleName = name;
    }
    
    public String getMotherLastName()
    {
        return motherLastName;
    }
    public void setMotherLastName(String name)
    {
        motherLastName = name;
    }
}
