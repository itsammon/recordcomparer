/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework1.person;

/**
 * Class to represent an adult
 * @author ammon
 */
public class Adult extends Person {
    private String phone1;
    private String phone2;
    
    public String getPhone1()
    {
        return phone1;
    }
    public void setPhone1(String number)
    {
        phone1 = number;
    }
    
    public String getPhone2()
    {
        return phone2;
    }
    public void setPhone2(String number)
    {
        phone2 = number;
    }
}
