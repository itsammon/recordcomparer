/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.homework1;

import ammonclegg.homework1.input.Parser;
import ammonclegg.homework1.matching.ByBirthday;
import ammonclegg.homework1.matching.ByFirstAndMiddleName;
import ammonclegg.homework1.matching.Children;
import ammonclegg.homework1.matching.Children2;
import ammonclegg.homework1.matching.CompareAdults;
import ammonclegg.homework1.matching.MatchStrategy;
import ammonclegg.homework1.output.ConsoleOutput;
import ammonclegg.homework1.output.MatchedPair;
import ammonclegg.homework1.output.OutputStrategy;
import ammonclegg.homework1.output.TextFileOutput;
import com.google.gson.Gson;
import homework1.person.Person;
import java.io.IOException;
import static java.lang.System.exit;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author Ammon Clegg
 */
public class Homework1 {
    private static final List<OutputStrategy> outputs = new ArrayList<>();
    private static List<MatchedPair> matches;
    private static List<Person> persons;
    private static final List<MatchStrategy> matchingStrategies = new ArrayList<>();
    private static String inFile;
    private static String inFileType;
    private static String outFile;
    
    /**
     * The main function of the program
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Parse the arguments
        parseArgs(args);
        
        // For debugging and testing purposes
        //inFile = "TestFiles/PersonTestSet_01.json";
        //inFileType = "json";
        //outFile = "Output.txt";
        
        // Setup the output strategies used here
        outputs.add(new ConsoleOutput());
        outputs.add(new TextFileOutput(outFile));
        
        setupStrategies();
        
        // Load in file of records
        persons = Parser.parseFile(inFile, inFileType);
        
        // Output list of persons for testing 
        // Allow user to know what was gotten
        Gson gson = new Gson();
        String json;
        for(Person person : persons)
        {
            json = gson.toJson(person);
            System.out.println(json);
            System.out.println(person.getClass());
        }
        
        // If there were people to test, test them.
        if (persons != null)
        {
            // Find matches
            findMatches();
        
            // Output to outputs
            outputMatches();
        }
    }
    
    /**
     * Parses the command line arguments
     * @param args The command line arguments
     */
    private static void parseArgs(String[] args)
    {
        Options options = new Options();
        
        options.addOption("h", "help", false, "print help menu");
        options.addOption("i", "in", true, "input file name");
        options.addOption("t", "type", true, "input file type");
        options.addOption("o", "out", true, "output file name");
        
        // Parse the command line
        DefaultParser parser = new DefaultParser();
        try {
            CommandLine line = parser.parse(options, args);
            
            // Create help
            if (line.hasOption("h"))
            {
                HelpFormatter helpformatter = new HelpFormatter();
                helpformatter.printHelp("homework1", options);
                exit(0);
            }
            
            // Get input file
            if (line.hasOption("i"))
            {
                inFile = line.getOptionValue("i");
            }
            else
            {
                throw new Exception("No input file given");
            }
            
            // Get input file type
            if (line.hasOption("t"))
            {
                inFileType = line.getOptionValue("t");
            }
            else
            {
                throw new Exception("No input file type given");
            }
            
            // Get output file
            if (line.hasOption("o"))
            {
                outFile = line.getOptionValue("o");
            }
            else
            {
                outFile = "Output.txt";
            }
            
        } catch (ParseException ex) {
            System.out.println(ex);
            System.err.println("Error parsing arguements!");
            HelpFormatter helpformatter = new HelpFormatter();
            helpformatter.printHelp("homework1", options);
            exit(-1);
        } catch (Exception ex)
        {
            System.err.println("Missing arguements");
            System.err.println("Requires -i, -t, and -o arguements");
            HelpFormatter helpformatter = new HelpFormatter();
            helpformatter.printHelp("homework1", options);
            exit(-1);
        }
        
    }
    
    /**
     * Sets up the strategies to use for determining matches
     */
    private static void setupStrategies()
    {
        matchingStrategies.add(new ByBirthday());
        matchingStrategies.add(new Children());
        matchingStrategies.add(new ByFirstAndMiddleName());
        matchingStrategies.add(new Children2());
        matchingStrategies.add(new CompareAdults());
    }
    
    /**
     * Run the test to find all the matches
     */
    private static void findMatches()
    {
        matches = new ArrayList<>();
        boolean isMatch;
        
        // Loop through all the persons to find all matches
        for (Person p1: persons)
        {
            for (Person p2: persons)
            {
                // If we are looking at the same object or a pair we already checked, jump out.
                if (p1.getObjectId() >= p2.getObjectId())
                {
                    continue;
                }

                // Run through all the strategies looking for a match
                for (MatchStrategy strat: matchingStrategies)
                {
                    // If they were a match, add them to the list
                    if (strat.match(p1, p2))
                    {
                        matches.add(new MatchedPair(p1.getObjectId(), p2.getObjectId()));
                        break; // Stop comparing if they are the same.
                    }
                }
            }
        }
    }
    
    /**
     * Outputs all the matches using all output strategies defined.
     */
    private static void outputMatches()
    {
        for (OutputStrategy out: outputs)
        {
            try
            {
                if (matches != null)
                {
                    out.output(matches);
                }
            }
            catch(IOException e)
            {
                System.out.println("Error writing to file!");
            }
        }
    }
}
