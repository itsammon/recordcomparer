/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.homework1.matching;

import homework1.person.Person;

/**
 *
 * @author ammon
 */
public class ByBirthday implements MatchStrategy{

    @Override
    public boolean match(Person person1, Person person2) {
        if (
                ((person1.getStateFileNumber() != null && person2.getStateFileNumber() != null) ||
                (person1.getSocialSecurityNumber() != null && person2.getSocialSecurityNumber() != null))
                &&
                person1.getBirthDay() != 0 && person2.getBirthDay() != 0 &&
                person1.getBirthMonth() != 0 && person2.getBirthMonth() != 0 &&
                person1.getBirthYear() != 0 && person2.getBirthYear() != 0)
        {
            if (person1.getStateFileNumber() != null && person2.getStateFileNumber() != null &&
                    !person1.getStateFileNumber().equals(person2.getStateFileNumber()))
            {
                return false;
            }
            if (person1.getSocialSecurityNumber()!= null && person2.getSocialSecurityNumber() != null &&
                    !person1.getSocialSecurityNumber().equals(person2.getSocialSecurityNumber()))
            {
                return false;
            }
            if (person1.getBirthDay() != person2.getBirthDay())
                return false;
            if (person1.getBirthMonth() != person2.getBirthMonth())
                return false;
            return person1.getBirthYear() == person2.getBirthYear();
        }
        else
            return false;
    }
    
}
