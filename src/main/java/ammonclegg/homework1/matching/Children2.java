/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.homework1.matching;

import homework1.person.Child;
import homework1.person.Person;

/**
 * Compares two people if they are children
 * @author ammon
 */
public class Children2 implements MatchStrategy{

    /**
     * Tries to match two people
     * @param person1 The first person
     * @param person2 The second person
     * @return true if they are a match
     */
    @Override
    public boolean match(Person person1, Person person2) {
       if (person1.getClass() == Child.class && person2.getClass() == Child.class)
       {
           // Convert to children for comparision purposes
           Child c1 = (Child)person1;
           Child c2 = (Child)person2;
           
           // Check if the two both have necessary information
           if (c1.getFirstName() != null && c2.getFirstName() != null
                   && c1.getLastName() != null && c2.getLastName() != null
                   && c1.getGender() != null && c2.getGender() != null
                   && c1.getBirthYear() != 0 && c2.getBirthYear() != 0
                   && c1.getBirthMonth() != 0 && c2.getBirthMonth() != 0
                   && c1.getBirthDay() != 0 && c2.getBirthDay() != 0
                   )
           {
               // Check if the names match
               if (!c1.getFirstName().equals(c2.getFirstName())
                       || !c1.getLastName().equals(c2.getLastName()))
               {
                   return false;
               }
               else
               {
                   // Check if the genders and birthday match
                   if (!c1.getGender().equals(c2.getGender())
                           || c1.getBirthYear() != c2.getBirthYear()
                           || c1.getBirthMonth() != c2.getBirthMonth()
                           || c1.getBirthDay() != c2.getBirthDay())
                   {
                       return false;
                   }
                   else
                   {
                       // Check if the mother's names are the same
                        if (c1.getMotherFirstName() != null && c2.getMotherFirstName() != null)
                            if (!c1.getMotherFirstName().equals(c2.getMotherFirstName()))
                                return false;
                        if (c1.getMotherMiddleName() != null && c2.getMotherMiddleName() != null)
                            if (!c1.getMotherMiddleName().equals(c2.getMotherMiddleName()))
                                return false;   
                        if (c1.getMotherLastName() != null && c2.getMotherLastName() != null)
                            if (!c1.getMotherLastName().equals(c2.getMotherLastName()))
                                return false;
                        
                        return true;
                   }   
               }
           }      
       }
       return false;
    }
}
