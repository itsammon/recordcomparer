/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.homework1.matching;

import homework1.person.Person;

/**
 * Interface for match strategy
 * @author ammon
 */
public interface MatchStrategy {
    public boolean match (Person person1, Person person2);
}
