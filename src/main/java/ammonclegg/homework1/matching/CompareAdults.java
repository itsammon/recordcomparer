/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.homework1.matching;

import homework1.person.Adult;
import homework1.person.Person;

/**
 * Compares adults
 * @author ammon
 */
public class CompareAdults implements MatchStrategy {

    /**
     * Compares two people to see if they match using phone numbers and full names.
     * @param person1 The first person
     * @param person2 The second person
     * @return true if they are a match
     */
    @Override
    public boolean match(Person person1, Person person2) {
     
       if (person1.getClass() == Adult.class && person2.getClass() == Adult.class)
       {
           // Convert to children for comparision purposes
           Adult a1 = (Adult)person1;
           Adult a2 = (Adult)person2;
           
           if (a1.getFirstName() != null && a2.getFirstName() != null
                    && a1.getMiddleName() != null && a2.getMiddleName() != null
                    && a1.getLastName() != null && a2.getLastName() != null)
            {
                if(a1.getFirstName().equals(a2.getFirstName()) 
                    && a1.getMiddleName().equals(a2.getMiddleName())
                    && a1.getLastName().equals(a2.getLastName()))
                {
                    if ((a1.getPhone1() != null && a2.getPhone1() != null) 
                            && a1.getPhone1().equals(a2.getPhone1()))
                    {
                        return true;
                    }
                    if ((a1.getPhone2() != null && a2.getPhone2() != null) 
                            && a1.getPhone2().equals(a2.getPhone2()))
                    {
                        return true;
                    }
                }
                return false;
            }
       }
       return false;
    }
}
