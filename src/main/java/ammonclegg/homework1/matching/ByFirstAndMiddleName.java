/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.homework1.matching;

import homework1.person.Person;

/**
 * Class to match based on first and middle names with social security or state file numbers
 * @author ammon
 */
public class ByFirstAndMiddleName implements MatchStrategy {
    
    /**
     * Tries to match two people
     * @param person1 The first person
     * @param person2 The second person
     * @return true if they are a match
     */
    @Override
    public boolean match(Person person1, Person person2) {
      if (
                ((person1.getStateFileNumber() != null && person2.getStateFileNumber() != null) ||
                (person1.getSocialSecurityNumber() != null && person2.getSocialSecurityNumber() != null))
                && person1.getFirstName() != null && person2.getFirstName() != null
                && person1.getMiddleName() != null && person2.getMiddleName() != null)
        {
            if (person1.getStateFileNumber() != null && person2.getStateFileNumber() != null &&
                    !person1.getStateFileNumber().equals(person2.getStateFileNumber()))
            {
                return false;
            }
            if (person1.getSocialSecurityNumber()!= null && person2.getSocialSecurityNumber() != null &&
                    !person1.getSocialSecurityNumber().equals(person2.getSocialSecurityNumber()))
            {
                return false;
            }
            return (person1.getFirstName().equals(person2.getFirstName()) 
                    && person1.getMiddleName().equals(person2.getMiddleName()));
        }
        else
            return false;
    }
}
