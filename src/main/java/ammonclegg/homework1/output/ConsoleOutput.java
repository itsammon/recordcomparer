/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.homework1.output;

import java.io.IOException;
import java.util.List;

/**
 * Outputs the results to the console
 * @author ammon
 */
public class ConsoleOutput implements OutputStrategy{
    
    /**
     * Outputs the list of matches to the console
     * @param pairs The pairs of matches
     * @throws IOException 
     */
    @Override
    public void output(List<MatchedPair> pairs) throws IOException {
        System.out.println("The matching pairs were: ");
        
        for(MatchedPair pair : pairs)
        {
            System.out.println("(" + pair.getPerson1() + "," + pair.getPerson2() + ")");
        }
    }
    
}
