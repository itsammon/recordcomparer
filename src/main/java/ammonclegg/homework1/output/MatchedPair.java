/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.homework1.output;

/**
 * Class to hold a matched pair of people
 * @author ammon
 */
public class MatchedPair {
    private final int person1;
    private final int person2;
    
    public MatchedPair(int object1, int object2)
    {
        person1 = object1;
        person2 = object2;
    }
    
    public int getPerson1()
    {
        return person1;
    }
    
    public int getPerson2()
    {
        return person2;
    }
}
