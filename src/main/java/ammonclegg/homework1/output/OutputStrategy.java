/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.homework1.output;

import java.io.IOException;
import java.util.List;

/**
 * Interface for the output strategies
 * @author ammon
 */
public interface OutputStrategy {
    /**
     * Function that outputs the list
     * @param pairs The matches
     * @throws IOException 
     */
    public void output (List<MatchedPair> pairs)
            throws IOException;
}
