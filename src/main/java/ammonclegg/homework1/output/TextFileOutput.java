/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.homework1.output;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Class that outputs to a text file
 * @author ammon
 */
public class TextFileOutput implements OutputStrategy{

    private final String outputFile;
    private final boolean _append;
    
    /**
     * Constructor
     * @param filename The filename to write to
     */
    public TextFileOutput(String filename)
    {
        outputFile = filename;
        _append = false;
    }
    
    /**
     * Constructor
     * @param filename The filename to write to
     * @param append Whether the program will append to the end of the file
     */
    public TextFileOutput(String filename, boolean append)
    {
        outputFile = filename;
        _append = append;
    }
    
    /**
     * Function to output the matches
     * @param pairs The matches
     * @throws IOException 
     */
    @Override
    public void output(List<MatchedPair> pairs) throws IOException {
        try (FileWriter writer = new FileWriter(outputFile, _append)) {
            writer.write("The matches were: \n");
            
            for (MatchedPair pair : pairs)
            {
                writer.write("(" + pair.getPerson1() + "," + pair.getPerson2() + ")\n");
            }
        }
    }
    
}
