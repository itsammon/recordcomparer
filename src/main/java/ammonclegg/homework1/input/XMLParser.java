/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.homework1.input;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import homework1.person.Person;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * XML Parser
 * @author ammon
 */
public class XMLParser implements InputStrategy {

    /**
     * Parses an xml file
     * @param filename The file to parse
     * @return A list of persons from the file
     * @throws FileNotFoundException
     * @throws IOException 
     */
    @Override
    public List<Person> parseFile(String filename) throws FileNotFoundException, IOException { 
        XmlMapper mapper = new XmlMapper();
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.PASCAL_CASE_TO_CAMEL_CASE);
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        System.out.println("Reading XML from a file");
            
        BufferedReader read = new BufferedReader(new FileReader(filename));
            
        // Deserialize the list of objects from json
        List<Person> persons = mapper.readValue(read, TypeFactory.defaultInstance().constructCollectionType(ArrayList.class, Person.class));
                
        return persons;
    }
    
}
