/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.homework1.input;

import homework1.person.Person;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author ammon
 */
public class Parser {
    private static InputStrategy input;
    public static List<Person> parseFile(String filename, String filetype)
    {
        // sets an appropriate parser
        switch(filetype.toLowerCase())
        {
            case "json":
                input = new JsonParser();
                break;
            case "xml":
                input = new XMLParser();
                break;
            default:
                System.out.println("Unknown file type!");
                return null;
        }
        
        // Parse using appropriate parser
        try
        {
            return input.parseFile(filename);
        }
        catch(FileNotFoundException e)
        {
            System.out.println("File not found!");
            return null;
        }
        catch(IOException e)
        {
            System.out.println("Error reading file!");
            return null;
        }
    }
}
