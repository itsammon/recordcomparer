/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.homework1.input;

import homework1.person.Person;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * Interface for all input strategies
 * @author ammon
 */
public interface InputStrategy {
    public List<Person> parseFile(String filename)
        throws FileNotFoundException, IOException;
}
