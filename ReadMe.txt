Uses maven for building jtest for unit tests
Built using Netbeans

To compile run "mvn compile" or "mvn clean install"
Run using "java -jar target/Homework1-1.0.jar -i <inputFile> -t <inputType> -o <outputFile>"

Files expect "__type" for type definitions see test files for examples

Uses jre 8
